﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BorderPanel2 = New Imext_Canvas_Mark_II.BorderPanel()
        Me.BorderPanel6 = New Imext_Canvas_Mark_II.BorderPanel()
        Me.BorderPanel7 = New Imext_Canvas_Mark_II.BorderPanel()
        Me.BorderPanel5 = New Imext_Canvas_Mark_II.BorderPanel()
        Me.BorderPanel4 = New Imext_Canvas_Mark_II.BorderPanel()
        Me.BorderPanel3 = New Imext_Canvas_Mark_II.BorderPanel()
        Me.eraserBorder = New Imext_Canvas_Mark_II.BorderPanel()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.brushBorder = New Imext_Canvas_Mark_II.BorderPanel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.BorderPanel1 = New Imext_Canvas_Mark_II.BorderPanel()
        Me.Canvas1 = New Imext_Canvas_Mark_II.Canvas()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.eraserBorder.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.brushBorder.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.Button1.Location = New System.Drawing.Point(17, 295)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(73, 23)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Generate"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.Label1.Location = New System.Drawing.Point(14, 321)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 14)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "0"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BorderPanel2
        '
        Me.BorderPanel2.BackColor = System.Drawing.Color.Lime
        Me.BorderPanel2.Border = System.Drawing.Color.Black
        Me.BorderPanel2.Location = New System.Drawing.Point(59, 187)
        Me.BorderPanel2.Name = "BorderPanel2"
        Me.BorderPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.BorderPanel2.Size = New System.Drawing.Size(25, 25)
        Me.BorderPanel2.TabIndex = 11
        '
        'BorderPanel6
        '
        Me.BorderPanel6.BackColor = System.Drawing.Color.DodgerBlue
        Me.BorderPanel6.Border = System.Drawing.Color.Black
        Me.BorderPanel6.Location = New System.Drawing.Point(59, 152)
        Me.BorderPanel6.Name = "BorderPanel6"
        Me.BorderPanel6.Padding = New System.Windows.Forms.Padding(1)
        Me.BorderPanel6.Size = New System.Drawing.Size(25, 25)
        Me.BorderPanel6.TabIndex = 9
        '
        'BorderPanel7
        '
        Me.BorderPanel7.BackColor = System.Drawing.Color.Yellow
        Me.BorderPanel7.Border = System.Drawing.Color.Black
        Me.BorderPanel7.Location = New System.Drawing.Point(23, 187)
        Me.BorderPanel7.Name = "BorderPanel7"
        Me.BorderPanel7.Padding = New System.Windows.Forms.Padding(1)
        Me.BorderPanel7.Size = New System.Drawing.Size(25, 25)
        Me.BorderPanel7.TabIndex = 10
        '
        'BorderPanel5
        '
        Me.BorderPanel5.BackColor = System.Drawing.Color.Red
        Me.BorderPanel5.Border = System.Drawing.Color.Black
        Me.BorderPanel5.Location = New System.Drawing.Point(23, 152)
        Me.BorderPanel5.Name = "BorderPanel5"
        Me.BorderPanel5.Padding = New System.Windows.Forms.Padding(1)
        Me.BorderPanel5.Size = New System.Drawing.Size(25, 25)
        Me.BorderPanel5.TabIndex = 8
        '
        'BorderPanel4
        '
        Me.BorderPanel4.BackColor = System.Drawing.Color.White
        Me.BorderPanel4.Border = System.Drawing.Color.Black
        Me.BorderPanel4.Location = New System.Drawing.Point(59, 118)
        Me.BorderPanel4.Name = "BorderPanel4"
        Me.BorderPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.BorderPanel4.Size = New System.Drawing.Size(25, 25)
        Me.BorderPanel4.TabIndex = 7
        '
        'BorderPanel3
        '
        Me.BorderPanel3.BackColor = System.Drawing.Color.Black
        Me.BorderPanel3.Border = System.Drawing.Color.Black
        Me.BorderPanel3.Location = New System.Drawing.Point(23, 117)
        Me.BorderPanel3.Name = "BorderPanel3"
        Me.BorderPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.BorderPanel3.Size = New System.Drawing.Size(25, 25)
        Me.BorderPanel3.TabIndex = 6
        '
        'eraserBorder
        '
        Me.eraserBorder.Border = System.Drawing.Color.Black
        Me.eraserBorder.Controls.Add(Me.PictureBox2)
        Me.eraserBorder.Location = New System.Drawing.Point(57, 46)
        Me.eraserBorder.Name = "eraserBorder"
        Me.eraserBorder.Padding = New System.Windows.Forms.Padding(1)
        Me.eraserBorder.Size = New System.Drawing.Size(32, 32)
        Me.eraserBorder.TabIndex = 5
        '
        'PictureBox2
        '
        Me.PictureBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(1, 1)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(30, 30)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'brushBorder
        '
        Me.brushBorder.Border = System.Drawing.Color.Red
        Me.brushBorder.Controls.Add(Me.PictureBox1)
        Me.brushBorder.Location = New System.Drawing.Point(19, 45)
        Me.brushBorder.Name = "brushBorder"
        Me.brushBorder.Padding = New System.Windows.Forms.Padding(1)
        Me.brushBorder.Size = New System.Drawing.Size(32, 32)
        Me.brushBorder.TabIndex = 4
        '
        'PictureBox1
        '
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(1, 1)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(30, 30)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'BorderPanel1
        '
        Me.BorderPanel1.BackColor = System.Drawing.Color.Black
        Me.BorderPanel1.Border = System.Drawing.Color.Black
        Me.BorderPanel1.Location = New System.Drawing.Point(39, 252)
        Me.BorderPanel1.Name = "BorderPanel1"
        Me.BorderPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.BorderPanel1.Size = New System.Drawing.Size(30, 30)
        Me.BorderPanel1.TabIndex = 1
        '
        'Canvas1
        '
        Me.Canvas1.GridSize = New System.Drawing.Size(16, 16)
        Me.Canvas1.Location = New System.Drawing.Point(116, 15)
        Me.Canvas1.Name = "Canvas1"
        Me.Canvas1.PrimaryColor = System.Drawing.Color.Black
        Me.Canvas1.SecondaryColor = System.Drawing.Color.White
        Me.Canvas1.SelectedTool = Imext_Canvas_Mark_II.Canvas.Tool.Brush
        Me.Canvas1.Size = New System.Drawing.Size(321, 321)
        Me.Canvas1.TabIndex = 0
        Me.Canvas1.Text = "Canvas1"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(19, 16)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(69, 23)
        Me.Button2.TabIndex = 13
        Me.Button2.Text = "New"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(19, 84)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(69, 23)
        Me.Button3.TabIndex = 14
        Me.Button3.Text = "Fill"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(23, 222)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(61, 23)
        Me.Button4.TabIndex = 15
        Me.Button4.Text = "225"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(310, 319)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(123, 13)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Made By VmX5 (Tommy)"
        Me.Label2.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(217, 152)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(123, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Made By VmX5 (Tommy)"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(448, 345)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BorderPanel2)
        Me.Controls.Add(Me.BorderPanel6)
        Me.Controls.Add(Me.BorderPanel7)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.BorderPanel5)
        Me.Controls.Add(Me.BorderPanel4)
        Me.Controls.Add(Me.BorderPanel3)
        Me.Controls.Add(Me.eraserBorder)
        Me.Controls.Add(Me.brushBorder)
        Me.Controls.Add(Me.BorderPanel1)
        Me.Controls.Add(Me.Canvas1)
        Me.Controls.Add(Me.Label3)
        Me.Name = "Form1"
        Me.Text = "Imext Canvas Mark II"
        Me.eraserBorder.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.brushBorder.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Canvas1 As Imext_Canvas_Mark_II.Canvas
    Friend WithEvents BorderPanel1 As Imext_Canvas_Mark_II.BorderPanel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents brushBorder As Imext_Canvas_Mark_II.BorderPanel
    Friend WithEvents eraserBorder As Imext_Canvas_Mark_II.BorderPanel
    Friend WithEvents BorderPanel3 As Imext_Canvas_Mark_II.BorderPanel
    Friend WithEvents BorderPanel4 As Imext_Canvas_Mark_II.BorderPanel
    Friend WithEvents BorderPanel5 As Imext_Canvas_Mark_II.BorderPanel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents BorderPanel6 As Imext_Canvas_Mark_II.BorderPanel
    Friend WithEvents BorderPanel2 As Imext_Canvas_Mark_II.BorderPanel
    Friend WithEvents BorderPanel7 As Imext_Canvas_Mark_II.BorderPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label

End Class
