﻿''' <summary>
''' Made by VmX5 (Tommy)
''' MIT License
''' </summary>

Public Class Form1

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        brushBorder.Border = Color.Red
        eraserBorder.Border = Color.Black
        Canvas1._selTool = Canvas.Tool.Brush
    End Sub

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        brushBorder.Border = Color.Black
        eraserBorder.Border = Color.Red
        Canvas1._selTool = Canvas.Tool.Eraser
    End Sub


    Private Sub BorderPanel1_Click(sender As Object, e As EventArgs) Handles BorderPanel1.Click
        Dim nCol As New ColorDialog
        If nCol.ShowDialog = Windows.Forms.DialogResult.OK Then
            BorderPanel1.BackColor = nCol.Color
        End If
        Canvas1.PrimaryColor = nCol.Color
        Canvas1.UpdateColors(BorderPanel1)
    End Sub

    Private Sub BorderPanel3_Click(sender As Object, e As EventArgs) Handles BorderPanel3.Click
        Canvas1.PrimaryColor = Color.Black
        Canvas1.UpdateColors(BorderPanel1)
    End Sub

    Private Sub BorderPanel4_Click(sender As Object, e As EventArgs) Handles BorderPanel4.Click
        Canvas1.PrimaryColor = Color.White
        Canvas1.UpdateColors(BorderPanel1)
    End Sub

    Private Sub BorderPanel5_Click(sender As Object, e As EventArgs) Handles BorderPanel5.Click
        Canvas1.PrimaryColor = Color.Red
        Canvas1.UpdateColors(BorderPanel1)
    End Sub

    Private Sub BorderPanel6_Click(sender As Object, e As EventArgs) Handles BorderPanel6.Click
        Canvas1.PrimaryColor = Color.DodgerBlue
        Canvas1.UpdateColors(BorderPanel1)
    End Sub

    Private Sub BorderPanel7_Click(sender As Object, e As EventArgs) Handles BorderPanel7.Click
        Canvas1.PrimaryColor = Color.Yellow
        Canvas1.UpdateColors(BorderPanel1)
    End Sub

    Private Sub BorderPanel2_Click(sender As Object, e As EventArgs) Handles BorderPanel2.Click
        Canvas1.PrimaryColor = Color.Lime
        Canvas1.UpdateColors(BorderPanel1)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Canvas1.Clear()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        output.Show()
        output.TextBox1.Text = Canvas1.ReturnOutput
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Canvas1.Fill()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If Button4.Text = "225" Then
            Button4.Text = "0"
        Else
            Button4.Text = "225"
        End If
    End Sub
End Class

Public Class Canvas
    Inherits Control

    Public drawnPixels As Integer = 0

    Dim mouseState As mState = mState.None 

    Dim drawnObjects As New List(Of Pixel) 

    Public _GridSize As Size = New Size(16, 16)
    Public Property GridSize As Size
        Get
            Return _GridSize
        End Get
        Set(value As Size)
            _GridSize = value
            Invalidate()
        End Set
    End Property

    Public _selTool As Tool = Tool.Brush
    Public Property SelectedTool As Tool
        Get
            Return _selTool
        End Get
        Set(value As Tool)
            _selTool = value
        End Set
    End Property

    Public _primaryCol As Color = Color.Black
    Public Property PrimaryColor As Color
        Get
            Return _primaryCol
        End Get
        Set(value As Color)
            _primaryCol = value
        End Set
    End Property

    Public _secondaryColor As Color = Color.White
    Public Property SecondaryColor As Color
        Get
            Return _secondaryColor
        End Get
        Set(value As Color)
            _secondaryColor = value
        End Set
    End Property

    Sub New()
        DoubleBuffered = True
    End Sub  

    Public Function ReturnOutput()
        Dim returnString As String = ""

        Dim organizedPixels As New List(Of Pixel)

        For y As Integer = 0 To GridSize.Height
            For x As Integer = 0 To GridSize.Width
                For Each pxl As Pixel In drawnObjects
                    If (pxl.Location.X = x) And (pxl.Location.Y = y) Then
                        Dim nPoint As New Pixel
                        nPoint.Location = New Point(x, y)
                        nPoint.Color = pxl.Color
                        organizedPixels.Add(nPoint)
                        Exit For
                    End If
                Next
            Next
        Next

        Dim mappedPixels As New List(Of Pixel)

        For Each pxl As Pixel In organizedPixels
            Dim nPxl As New Pixel
            nPxl.Location = pxl.Location
            nPxl.Color = Color.FromArgb(map(pxl.Color.A, 0, 255, 32, 126), map(pxl.Color.R, 0, 255, 32, 126), map(pxl.Color.G, 0, 255, 32, 126), map(pxl.Color.B, 0, 255, 32, 126))
            mappedPixels.Add(nPxl)
        Next

        Dim tileStrings As New List(Of String)
        Dim x10Line As New List(Of String)
        Dim finalString As String = ""

        For y As Integer = 1 To GridSize.Height
            For x As Integer = 1 To GridSize.Width
                For Each scanPixel As Pixel In mappedPixels
                    If scanPixel.Location = New Point(x, y) Then
                        tileStrings.Add(Chr(scanPixel.Color.A) & Chr(scanPixel.Color.R) & Chr(scanPixel.Color.G) & Chr(scanPixel.Color.B))
                        Exit For
                    End If
                Next
            Next
        Next

        For Each Str As String In tileStrings
            For i As Integer = 1 To 10
                x10Line.Add(Str)
            Next
        Next

        Dim finalLines As New List(Of String)
        For Each Str As String In x10Line
            finalLines.Add(Str)
        Next

        Dim finalColorLines As New List(Of String)
        For Each line As String In finalLines
            finalString &= line
            If finalString.ToString.Length = (GridSize.Width * 10 * 4) Then
                finalColorLines.Add(finalString)
                finalString = ""
            End If
        Next

        For Each line As String In finalColorLines
            returnString &= String.Join("", Enumerable.Repeat(line, 10))
        Next

        Return returnString
    End Function

    Function map(input As Integer, inMin As Integer, inMax As Integer, outMin As Integer, outMax As Integer)
        Return (input - inMin) * (outMax - outMin) / (inMax - inMin) + outMin
    End Function

    Protected Overrides Sub OnPaint(e As PaintEventArgs)
        MyBase.OnPaint(e)
        Dim g As Graphics = e.Graphics
        g.DrawRectangle(Pens.Black, 0, 0, Width - 1, Height - 1)
        For x As Integer = 1 To _GridSize.Width
            g.DrawLine(Pens.Black, New Point(0, 20 * x), New Point(Width, 20 * x))
        Next
        For y As Integer = 1 To _GridSize.Height
            g.DrawLine(Pens.Black, New Point(20 * y, 0), New Point(20 * y, Height))
        Next

        For Each pxl As Pixel In drawnObjects
            g.FillRectangle(New SolidBrush(pxl.Color), (pxl.Location.X - 1) * 20 + 1, (pxl.Location.Y - 1) * 20 + 1, 19, 19)
        Next

        Form1.Label1.Text = drawnObjects.Count.ToString
    End Sub

    Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
        MyBase.OnMouseDown(e)
        mouseState = mState.Down
    End Sub 

    Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
        MyBase.OnMouseUp(e)
        mouseState = mState.Up
    End Sub

    Protected Overrides Sub OnMouseLeave(e As EventArgs)
        MyBase.OnMouseLeave(e)
        mouseState = mState.None
    End Sub

    Protected Overrides Sub OnResize(e As EventArgs)
        MyBase.OnResize(e)
        Invalidate()
    End Sub

    Protected Overrides Sub OnMouseMove(e As MouseEventArgs)
        MyBase.OnMouseMove(e)
        If mouseState = mState.Down Then
            For x As Integer = 0 To GridSize.Width - 1
                For y As Integer = 0 To GridSize.Height - 1
                    If New Rectangle(x * 20, y * 20, 20, 20).Contains(e.X, e.Y) Then
                        If _selTool = Tool.Brush Then
                            DrawPixel(New Point(x + 1, y + 1))
                        ElseIf _selTool = Tool.Eraser Then
                            ErasePixel(New Point(x + 1, y + 1))
                        End If
                    End If
                Next
            Next
        End If 
    End Sub

    Sub DrawPixel(selectedPixel As Point)  
        If drawnObjects.Count < 1 Then
            Dim nPixel As New Pixel
            nPixel.Color = (Color.FromArgb(Convert.ToInt32(Form1.Button4.Text), PrimaryColor))
            nPixel.Location = New Point(selectedPixel)
            drawnObjects.Add(nPixel)
        Else
            Dim matchFound As Pixel = Nothing
            For Each pxl As Pixel In drawnObjects
                If pxl.Location = New Point(selectedPixel.X, selectedPixel.Y) Then
                    matchFound = pxl
                    Exit For
                End If
            Next
            If matchFound IsNot Nothing Then
                ErasePixel(matchFound.Location)
                Dim nPixel As New Pixel
                nPixel.Color = (Color.FromArgb(Convert.ToInt32(Form1.Button4.Text), PrimaryColor))
                nPixel.Location = New Point(selectedPixel)
                drawnObjects.Add(nPixel)
            Else
                Dim nPixel As New Pixel
                nPixel.Color = (Color.FromArgb(Convert.ToInt32(Form1.Button4.Text), PrimaryColor))
                nPixel.Location = New Point(selectedPixel)
                drawnObjects.Add(nPixel)
            End If
        End If
        Invalidate()
    End Sub

    Sub ErasePixel(selectedPixel As Point)
        For Each pxl As Pixel In drawnObjects
            If pxl.Location = selectedPixel Then
                drawnObjects.Remove(pxl)
                Exit For
            End If
        Next
        Invalidate()
    End Sub

    Sub UpdateColors(ctrl As Control)
        ctrl.BackColor = PrimaryColor
    End Sub

    Sub Clear() 
        drawnObjects.Clear()
        Invalidate()
    End Sub

    Sub Fill()
        drawnObjects.Clear()
        For y As Integer = 1 To GridSize.Height
            For x As Integer = 1 To GridSize.Width
                Dim nPxl As New Pixel
                nPxl.Color = (Color.FromArgb(Convert.ToInt32(Form1.Button4.Text), PrimaryColor))
                nPxl.Location = New Point(x, y)
                drawnObjects.Add(nPxl)
            Next
        Next
        Invalidate()
    End Sub

    Enum mState
        None = 1
        Down = 2
        Up = 3
    End Enum

    Public Enum Tool
        Brush = 1
        Eraser = 2
    End Enum

    Public Class Pixel
        Public Location As Point
        Public Color As Color
    End Class
End Class
 
Public Class BorderPanel
    Inherits Panel

    Public _border As Color = Color.Black
    Public Property Border As Color
        Get
            Return _border
        End Get
        Set(value As Color)
            _border = value
            Invalidate()
        End Set
    End Property

    Sub New()
        DoubleBuffered = True
        Padding = New Padding(1, 1, 1, 1)
    End Sub

    Protected Overrides Sub OnPaint(e As PaintEventArgs)
        MyBase.OnPaint(e)
        e.Graphics.DrawRectangle(New Pen(_border), 0, 0, Width - 1, Height - 1)
    End Sub
End Class 